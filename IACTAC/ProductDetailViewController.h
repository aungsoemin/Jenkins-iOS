//
//  ProductDetailViewController.h
//  IACTAC
//
//  Created by Zayar on 11/13/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"
#import "NXViewController.h"
@interface ProductDetailViewController : NXViewController

@property (nonatomic,retain) Products * objProduct;
@end
