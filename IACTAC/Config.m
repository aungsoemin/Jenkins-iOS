//
//  Config.m
//  fyre
//
//  Created by Zayar on 3/7/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Config.h"
#import "StringTable.h"

@implementation Config

@dynamic app_id;
@dynamic app_tint_color;
@dynamic base_bg;
@dynamic can_order;
@dynamic conf_link;
@dynamic nav_bg_color;
@dynamic nav_is_transparent;
@dynamic nav_logo_link;
@dynamic nav_title;
@dynamic nav_title_color;
@dynamic order_link;
@dynamic tab_selected_color;
+ (Config*)getCurrentDefaultConfig{
    static Config *_sharedClient = nil;
    
    //_sharedClient = [[Config alloc] init];
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
    NSUserDefaults * prefe = [NSUserDefaults standardUserDefaults];
    NSString * strAppId = [prefe objectForKey:APP_TITLE];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"app_id == %@",strAppId];
    _sharedClient = [Config findFirstWithPredicate:predicate inContext:localContext];
    if(!_sharedClient) {
        NSLog(@"config is nil. created entity");
        _sharedClient = [Config createEntity];
    }
    return _sharedClient;
}
@end
