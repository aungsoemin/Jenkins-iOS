//
//  ContactViewController.m
//  IACTAC
//
//  Created by Zayar on 10/29/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "ContactViewController.h"
#import "ContactCell.h"
#import "Utility.h"
#import "StringTable.h"
#import "IacAPIClient.h"
#import "Config_Tabs.h"
@interface ContactViewController ()
{
    QEntryElement * firstNameElement;
    QEntryElement * lastNameElement;
    
    QEntryElement * emailElement;
    QEntryElement * phoneElement;
    
    QMultilineElement * messageElement;
    QAppearance * contactCustomAppearance;
    
    NSString * strFName;
    NSString * strLName;
    NSString * strEmail;
    NSString * strPhone;
    NSString * strMessage;
    
    IBOutlet UITableView * tbl;
    UITextView * txtMessage;
    
    IBOutlet UIToolbar * toolbar;
    
}
@property (nonatomic,strong) QuickDialogController * qdController;
@end

@implementation ContactViewController
#define animation_delay 0.5
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([tbl respondsToSelector:@selector(separatorInset)]) {
        [tbl setSeparatorInset:UIEdgeInsetsZero];
    }
    
    UIBarButtonItem * sendItem = [[UIBarButtonItem alloc] initWithTitle:@"Send" style:UIBarButtonItemStylePlain target:self action:@selector(onSend:)];
    //sendItem.tintColor = [UIColor redColor];
    self.navigationItem.rightBarButtonItem = sendItem;
    
    /*if ([tbl respondsToSelector:@selector(separatorInset)]) {
        [tbl setSeparatorInset:UIEdgeInsetsZero];
    }*/
    
    txtMessage = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, 320, 120)];
    txtMessage.font = [UIFont fontWithName:@"AvenirNext-Medium" size:16];
    txtMessage.textColor = [UIColor colorWithHexString:@"6C6C6C"];
    txtMessage.delegate = self;

    
    //UIView * v = [[UIView alloc] init];
    //[tbl setSectionFooterHeight:0];
    
    //---registers the notifications for keyboard---
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    tbl.backgroundColor = [UIColor colorWithHexString: @"f0f0f0"];
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"contact view appear!!");
    [tbl reloadData];
}

- (void)onSend:(UIBarButtonItem *)sender{

    NSLog(@"Send!!!");
    [self validateTheFieldAndSend];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ContactCell";
    static NSString *CellIdentifierText = @"ContactCellTextView";
	ContactCell *cell = (ContactCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[ContactCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    [cell loadTextWith:@"First Name" andPlaceHolder:@"first name"];
    [cell.txtValue setDelegate:self];
    
    //Service * obj = [arrCart objectAtIndex:indexPath.row];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [cell loadTextWith:@"First Name" andPlaceHolder:@"first name"];
            cell.txtValue.tag = 1;
            cell.txtValue.returnKeyType = UIReturnKeyNext;
        }
        else if(indexPath.row == 1){
            [cell loadTextWith:@"Last Name" andPlaceHolder:@"last name"];
            cell.txtValue.tag = 2;
            cell.txtValue.returnKeyType = UIReturnKeyNext;
        }
    }
    else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [cell loadTextWith:@"Email" andPlaceHolder:@"email"];
            cell.txtValue.tag = 3;
            cell.txtValue.returnKeyType = UIReturnKeyNext;
        }
        else if(indexPath.row == 1){
            [cell loadTextWith:@"Phone" andPlaceHolder:@"phone"];
            cell.txtValue.tag = 4;
            cell.txtValue.returnKeyType = UIReturnKeyNext;
        }
    }
    else if (indexPath.section == 2) {
        UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell2 == nil) {
            cell2 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierText];
        }
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        cell2.tag = indexPath.row;
        txtMessage.tag = 5;
        txtMessage.delegate = self;
        [cell2 addSubview:txtMessage];
        txtMessage.returnKeyType = UIReturnKeyDone;
        return cell2;
    }
    
    cell.txtValue.delegate = self;
    return cell;
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2) {
        return 120;
    }
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*if (newsDetailViewController == nil) {
        newsDetailViewController = [[NewsDetailViewController alloc] init];
    }
    News * obj = [arr objectAtIndex:indexPath.row];
    [newsDetailViewController loadTheViewWithNews:obj];
    [self.navigationController pushViewController:newsDetailViewController animated:YES];
    [self.navigationItem.backBarButtonItem setTintColor:[UIColor redColor]];*/
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    #warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    if (section == 0) {
        return 2;
    }
    else if(section == 1){
        return 2;
    }
    else if(section == 2){
        return 1;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    #warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CGRect newFrame = v.frame;
    newFrame.origin.x = 16;
    newFrame.origin.y = 25;
    newFrame.size.height = 20;
    UILabel * lbl = [[UILabel alloc] initWithFrame:newFrame];
    lbl.font = [UIFont fontWithName:@"AvenirNext-Regular" size:14];
    lbl.textColor = [UIColor colorWithHexString:@"6C6C6C"];
    lbl.backgroundColor = [UIColor clearColor];
    [v setBackgroundColor:[UIColor colorWithHexString:@"f0f0f0"]];
    
    if (section == 0) {
        lbl.text = @" PERSONAL";
    }
    else if(section == 1){
        lbl.text = @" CONTACT";
    }
    else if(section == 2){
        lbl.text = @" MESSAGE";
    }
    
    [v addSubview:lbl];
    [Utility makeBorder:v andWidth:0.5 andColor:[UIColor lightGrayColor]];
    return v;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

-(void) keyboardWillShow:(NSNotification *) notification {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect frame = toolbar.frame;
    if ([Utility isGreaterOREqualOSVersion:@"7.0"] ) frame.origin.y = self.view.frame.size.height - 260.0;
    else frame.origin.y = self.view.frame.size.height - 260.0+44+5;
    toolbar.frame = frame;
    
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *) notification {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect frame = toolbar.frame;
    frame.origin.y = self.view.frame.size.height;
    toolbar.frame = frame;
    
    [UIView commitAnimations];
    
    [self reMoveToTopTable:0];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    [self moveToTopTable:textView.tag];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag == 4 ) {
        NSLog(@"begin edited!! tag %d",textField.tag);
        [self moveToTopTable:textField.tag];
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    if(textField.tag == 1) {
        UIView * nextView = [self.view viewWithTag:2];
        [nextView becomeFirstResponder];
        return NO;
    }
    else if(textField.tag == 2) {
        UIView * nextView = [self.view viewWithTag:3];
        [nextView becomeFirstResponder];
        return NO;
    }
    else if(textField.tag == 3) {
        UIView * nextView = [self.view viewWithTag:4];
        [nextView becomeFirstResponder];
        return NO;
    }
    else if(textField.tag == 4) {
        NSLog(@"here is tag 4!!");
        UIView * nextView = [self.view viewWithTag:5];
        [nextView becomeFirstResponder];
        return NO;
    }
    
    [textField resignFirstResponder];
	return YES;
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"should start!!");
    

    return YES;
}

- (void) textViewDidEndEditing:(UITextView *)textView{
    [self reMoveToTopTable:textView.tag];
}

- (BOOL) textViewShouldEndEditing:(UITextView *)textView{
    return YES;
}

- (void)moveToTopTable:(int)tag{
    //if (tag == 5) {
        [UIView animateWithDuration:animation_delay animations:^{
            [tbl setFrame:CGRectMake(0, -180, tbl.frame.size.width, tbl.frame.size.height)];
        }];
    //}
}

- (void)reMoveToTopTable:(int)tag{
    //if (tag == 5) {
        [UIView animateWithDuration:animation_delay animations:^{
            [tbl setFrame:CGRectMake(0,0, tbl.frame.size.width, tbl.frame.size.height)];
        }];
    //}
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        NSLog(@"return!!");
        [textView resignFirstResponder];
        [self reMoveToTopTable:textView.tag];
        return NO;
    }
    
    return YES;
}

- (void)validateTheFieldAndSend{
    NSIndexPath * indexFName = [NSIndexPath indexPathForRow:0 inSection:0];
    ContactCell * fNameCell = (ContactCell *)[tbl cellForRowAtIndexPath:indexFName];
    if ([Utility stringIsEmpty:fNameCell.txtValue.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:ERRMSG1];
        strFName = @"";
        return;
    }
    else{
        strFName = fNameCell.txtValue.text;
    }
    NSIndexPath * indexLName = [NSIndexPath indexPathForRow:1 inSection:0];
    ContactCell * lNameCell = (ContactCell *)[tbl cellForRowAtIndexPath:indexLName];
    if ([Utility stringIsEmpty:lNameCell.txtValue.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:ERRMSG2];
        strLName = @"";
        return;
    }
    else strLName = lNameCell.txtValue.text;
    
    NSIndexPath * indexEmail = [NSIndexPath indexPathForRow:0 inSection:1];
    ContactCell * emailCell = (ContactCell *)[tbl cellForRowAtIndexPath:indexEmail];
    if ([Utility stringIsEmpty:emailCell.txtValue.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:ERRMSG12];
        strEmail = @"";
        return;
    }
    else if (![Utility validateEmailWithString:emailCell.txtValue.text]) {
        [Utility showAlert:APP_TITLE message:ERRMSG11];
        strEmail = @"";
        return;
    }
    else strEmail = emailCell.txtValue.text;
    
    NSIndexPath * indexPhone = [NSIndexPath indexPathForRow:1 inSection:1];
    ContactCell * phoneCell = (ContactCell *)[tbl cellForRowAtIndexPath:indexPhone];
    if ([Utility stringIsEmpty:phoneCell.txtValue.text shouldCleanWhiteSpace:YES]) {
        //[Utility showAlert:APP_TITLE message:ERRMSG10];
        //return;
        strPhone = @"";
    }
    else strPhone = phoneCell.txtValue.text;
    
    if ([Utility stringIsEmpty:txtMessage.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:APP_TITLE message:EPRMSG13];
        strMessage = @"";
        return;
    }
    else strMessage = txtMessage.text;
    
    [self onSend];
}

-(IBAction)resignKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (void) onSend{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tab_id == %@",TAB_CONTACT_ID];
    Config_Tabs * tab = [Config_Tabs MR_findFirstWithPredicate:predicate];
    if (![Utility stringIsEmpty:tab.api_url shouldCleanWhiteSpace:YES]) {
        [SVProgressHUD show];
        NSDictionary * param = [[NSDictionary alloc] init];
        param = @{@"firstName":strFName,@"lastName":strLName,@"email":strEmail,@"phone":strPhone, @"message":strMessage};
        
        [[IacAPIClient sharedClientWithoutBaseUrl] POST:[NSString stringWithFormat:@"%@",tab.api_url] parameters:param success:^(AFHTTPRequestOperation *operation, id json) {
            NSLog(@"successfully return!!! %@",json);
            NSDictionary * dics = (NSDictionary *)json;
            int status= [[dics objectForKey:@"status"] intValue];
            if (status == 1) {
                [SVProgressHUD showSuccessWithStatus:@"Successfully sent! We'll get back to you as soon as we can. Thanks for contacting us."];
                [tbl reloadData];
                txtMessage.text = @"";
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error %@",error);
            //[SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
            [SVProgressHUD dismiss];
        }];
    }
    
    
}

@end
