//
//  Config.h
//  fyre
//
//  Created by Zayar on 3/7/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Config : NSManagedObject

@property (nonatomic, retain) NSString * app_id;
@property (nonatomic, retain) NSString * app_tint_color;
@property (nonatomic, retain) NSString * base_bg;
@property (nonatomic, retain) NSNumber * can_order;
@property (nonatomic, retain) NSString * conf_link;
@property (nonatomic, retain) NSString * nav_bg_color;
@property (nonatomic, retain) NSNumber * nav_is_transparent;
@property (nonatomic, retain) NSString * nav_logo_link;
@property (nonatomic, retain) NSString * nav_title;
@property (nonatomic, retain) NSString * nav_title_color;
@property (nonatomic, retain) NSString * order_link;
@property (nonatomic, retain) NSString * tab_selected_color;

+ (Config*)getCurrentDefaultConfig;

@end
