//
//  CategoryListingViewController.h
//  fyre
//
//  Created by Zayar on 3/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NXViewController.h"
#import "Product_Category.h"
@interface CategoryListingViewController : NXViewController
@property (nonatomic, strong) Product_Category * product_cate;
@end
