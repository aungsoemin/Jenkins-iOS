//
//  StringTable.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface StringTable : NSObject {
    
}

extern NSString * const DBNAME;
extern NSString * const APP_TITLE;
extern NSString * const APP_ID;

extern NSString * const CONFIG_LINK;
extern NSString * const BASE_LINK;
extern NSString * const CONTACT_LINK;
extern NSString * const ORDER_LINK;
extern NSString * const NEWS_LINK;
extern NSString * const STORE_LINK;
extern NSString * const PRODUCT_LINK;

extern NSString * const TAB_PRODUCT_ID;
extern NSString * const TAB_STORE_ID;
extern NSString * const TAB_NEWS_ID;
extern NSString * const TAB_INFO_ID;
extern NSString * const TAB_CONTACT_ID;

extern NSString * const CRYPT_PASS_CODE;

extern NSString * const REPEAT_TYPE_NEVER;
extern NSString * const REPEAT_TYPE_EVER;
extern NSString * const REPEAT_TYPE_ONCE;

extern NSString * const ANSWER_TYPE_IMAGE;
extern NSString * const ANSWER_TYPE_VIDEO;
extern NSString * const ANSWER_TYPE_TEXT;
extern NSString * const ANSWER_TYPE_NULL;

extern NSString * const ANSWER_STATUS_PENDING;
extern NSString * const ANSWER_STATUS_ANSWERED;
extern NSString * const ANSWER_STATUS_REJECTED;
extern NSString * const LOGOUT_LINK;
extern NSString * const APN_SERVER_PATH;

extern NSString * const PAYMENT_CASH;
extern NSString * const PAYMENT_NETS;
extern NSString * const PAYMENT_VISA;
extern NSString * const PAYMENT_AMEX;

extern NSString * const NEWS_CATE_VIDEO;

extern NSString * const GENDER_STRING;

extern int const CARD_WIDTH;
extern int const CARD_HEIGHT;

extern int const STATUS_ACTION_SUCCESS;
extern int const STATUS_RETURN_RECORD;
extern int const STATUS_ACTION_FAILED;
extern int const STATUS_SESSION_EXPIRED;
extern int const STATUS_NO_RECORD_FOUND;

extern int const STATUS_REFUND_NO;
extern int const STATUS_REFUND_FULL;
extern int const STATUS_REFUND_PARTIAL;
extern int const STATUS_REFUND_ABSOLUTE;
extern int const STATUS_REFUND_PARTIAL_ABSOLUTE;

extern int const STATUS_DISCOUNT_PERCENTAGE;
extern int const STATUS_DISCOUNT_AMOUNT;
extern int const STATUS_DISCOUNT_NONE;

extern float const DEFUALT_TAX;
extern float const DEFUALT_DISCOUNT;
extern BOOL const DEFUALT_SYNC;

extern NSString * const ERRMSG1;
extern NSString * const ERRMSG2;
extern NSString * const ERRMSG3;
extern NSString * const ERRMSG4;
extern NSString * const ERRMSG5;
extern NSString * const ERRMSG6;
extern NSString * const ERRMSG7;
extern NSString * const ERRMSG8;
extern NSString * const ERRMSG9;
extern NSString * const ERRMSG10;
extern NSString * const ERRMSG11;
extern NSString * const ERRMSG12;
extern NSString * const EPRMSG13;

extern double const CACHE_DURATION;
extern double const PROFILE_IMG_VIEW_HEIGHT;
extern double const PROFILE_IMG_VIEW_WIDTH;

extern int const NORMAL_KEYPAD;
extern int const DECIMAL_KEYPAD;

extern int const YEARLY_TIMESTAMP;

extern int const DISCOUNT_TYPE_PERCENTAGE;
extern int const DISCOUNT_TYPE_AMOUNT;
@end
