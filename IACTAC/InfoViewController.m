//
//  InfoViewController.m
//  IACTAC
//
//  Created by Zayar on 10/29/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "InfoViewController.h"
#import "StringTable.h"
#import "Config_Tabs.h"
#import "Utility.h"
@interface InfoViewController ()
{
    IBOutlet UIWebView * webInfoView;
    IBOutlet UIImageView * imgARRLogoView;
}
@end

@implementation InfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [imgARRLogoView setImage:[UIImage imageNamed:@"ARR_logo_black"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tab_id == %@",TAB_INFO_ID];
    Config_Tabs * tab = [Config_Tabs MR_findFirstWithPredicate:predicate];
    if (![Utility stringIsEmpty:tab.api_url shouldCleanWhiteSpace:YES]) {
        //NSString *urlAddress = @"http://www.google.com";
        NSURL *url = [NSURL URLWithString:tab.api_url];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [webInfoView loadRequest:requestObj];
    }
    /*NSString * strPath = [[NSBundle mainBundle] pathForResource:@"info" ofType:@"html"];
    //[webView loadHTMLString:strPath baseURL:nil];
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    NSString* htmlString = [NSString stringWithContentsOfFile:strPath encoding:NSUTF8StringEncoding error:nil];*/
    //[webInfoView loadHTMLString:htmlString baseURL:baseURL];
}

@end
