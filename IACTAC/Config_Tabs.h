//
//  Config_Tabs.h
//  fyre
//
//  Created by Zayar on 3/7/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Config_Tabs : NSManagedObject

@property (nonatomic, retain) NSString * api_url;
@property (nonatomic, retain) NSString * app_id;
@property (nonatomic, retain) NSString * icon;
@property (nonatomic, retain) NSNumber * order_id;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSString * tab_id;

@end
