//
//  CustomScrollSliderView.h
//  IACTAC
//
//  Created by Zayar on 10/31/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfinitePagingView.h"
typedef enum {
	ScrollViewModeNotInitialized2,           // view has just been loaded
	ScrollViewModePaging2,                   // fully zoomed out, swiping enabled
	ScrollViewModeZooming2,                  // zoomed in, panning enabled
} ScrollViewMode2;
@interface CustomScrollSliderView : UIView
{
    UIScrollView * scrollView;
    UIPageControl * pageControl;
    ScrollViewMode2 scrollViewMode2;
    BOOL pageControlUsed;
    int currentPagingIndex;
    NSMutableArray * arrContentView;
    NSTimer * myTimer;
    int slidingIndex;
    InfinitePagingView * infinitePageView;
    int totalCount;
    NSInteger _lastPageIndex;
}
- (void) loadImageViewWith:(NSArray *)arr;
@end
