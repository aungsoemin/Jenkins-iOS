//
//  Stores.m
//  IACTAC
//
//  Created by Zayar on 12/27/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "Stores.h"


@implementation Stores

@dynamic content;
@dynamic date;
@dynamic email;
@dynamic fb_link;
@dynamic has_sync;
@dynamic image_url;
@dynamic is_active;
@dynamic lat;
@dynamic local_id;
@dynamic location;
@dynamic longtitude;
@dynamic order;
@dynamic phone;
@dynamic server_id;
@dynamic thumb;
@dynamic timetick;
@dynamic title;
@dynamic updated_time;
@dynamic fb_id;

@end
