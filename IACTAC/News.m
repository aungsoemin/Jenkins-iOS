//
//  News.m
//  IACTAC
//
//  Created by Zayar on 10/29/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "News.h"


@implementation News

@dynamic server_id;
@dynamic create_time;
@dynamic has_sync;
@dynamic is_active;
@dynamic local_id;
@dynamic timetick;
@dynamic updated_time;
@dynamic title;
@dynamic author;
@dynamic date;
@dynamic short_descript;
@dynamic content;
@dynamic image_url;
@dynamic url;
@dynamic category;

-(void)setTheDateFromString:(NSString *)strDate{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    //02-Jul-2013 21:00
    [formatter setDateFormat:@"dd-MMM-yyyy HH:mm"];
    self.date = [formatter dateFromString:strDate];
}

@end
