//
//  ContactViewController.h
//  IACTAC
//
//  Created by Zayar on 10/29/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NXViewController.h"

@interface ContactViewController : NXViewController<UITextViewDelegate>
{
}
@end
