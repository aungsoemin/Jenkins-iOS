//
//  Product_Category.m
//  IACTAC
//
//  Created by Zayar on 10/31/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "Product_Category.h"


@implementation Product_Category

@dynamic date;
@dynamic image_url;
@dynamic is_active;
@dynamic local_id;
@dynamic server_id;
@dynamic thumb;
@dynamic timetick;
@dynamic updated_time;
@dynamic name;

@end
