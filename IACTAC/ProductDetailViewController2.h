//
//  ProductDetailViewController2.h
//  IACTAC
//
//  Created by Zayar on 12/1/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"
#import "NXViewController.h"
@interface ProductDetailViewController2 : NXViewController
- (void)loadTheView:(Products *)obj;
@property (nonatomic, strong) Products * objProduct;
@end
