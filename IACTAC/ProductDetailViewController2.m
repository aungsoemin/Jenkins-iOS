//
//  ProductDetailViewController2.m
//  IACTAC
//
//  Created by Zayar on 12/1/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "ProductDetailViewController2.h"
#import "Products.h"
#import "MDCParallaxView.h"
#import <TTTAttributedLabel/TTTAttributedLabel.h>
#import "UIImageView+AFNetworking.h"
#import "OrderViewController.h"
#import "StringTable.h"
#import "Config.h"

@interface ProductDetailViewController2 ()
{
    UIImageView *backgroundImageView;
    MDCParallaxView *parallaxView;
    OrderViewController * orderViewController;
    UIBarButtonItem * btnOrder;
}
@end

@implementation ProductDetailViewController2
@synthesize objProduct;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    btnOrder = [[UIBarButtonItem alloc] initWithTitle:@"Order" style:UIBarButtonItemStylePlain target:self action:@selector(onOrder:)];
    self.navigationItem.rightBarButtonItem = btnOrder;
    
}

- (void)viewDidAppear:(BOOL)animated{
    //Config * config  = [Config getCurrentDefaultConfig];
    
    if ([objProduct.can_order intValue] == 1) {
        btnOrder.enabled = YES;
    }
    else{
        btnOrder.enabled = NO;
    }
    
    //self.navigationItem.backBarButtonItem.title = @"Back";
}

- (void)onOrder:(UIBarButtonItem *)sender{
        if (orderViewController == nil) {
            orderViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderViewController"];
           }
    orderViewController.objProduct = objProduct;
    [self.navigationController pushViewController:orderViewController animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [self loadTheView:self.objProduct];
    self.navigationController.navigationBar.topItem.backBarButtonItem = [[UIBarButtonItem alloc]
                                                                         initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)loadTheView:(Products *)obj{
    
    self.objProduct = obj;
    CGRect backgroundRect = CGRectMake(0, 0, 480, 404);
    backgroundImageView = [[UIImageView alloc] initWithFrame:backgroundRect];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    [backgroundImageView setImageWithURL:[NSURL URLWithString:obj.image_url] placeholderImage:[UIImage imageNamed:@"background.png"]];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [backgroundImageView addGestureRecognizer:tapGesture];
    
    CGRect textContainerRect = CGRectMake(0, 0, self.view.frame.size.width, 400.0f);
    //CGRect textRect = CGRectMake(20, 10, textContainerRect.size.width - 40, textContainerRect.size.height - 40);
    CGRect textRect = CGRectMake(0, 0, textContainerRect.size.width , textContainerRect.size.height - 40);
    
    UIView * textViewContainer = [[UIView alloc] initWithFrame: textContainerRect];
    textViewContainer.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    
    UIWebView * webView=[[UIWebView alloc] initWithFrame: textRect];
    [webView setOpaque:NO];
    [[webView scrollView]setBounces:NO];
    [[webView scrollView]setScrollEnabled:NO];
    [[webView scrollView] setShowsVerticalScrollIndicator:NO];
	[webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"AvenirNext-Regular\" size=\"2\" color='#000'> <br/> %@ </font></body></html>",obj.content] baseURL:nil];
    [webView setBackgroundColor:[UIColor whiteColor]];
    [textViewContainer addSubview: webView];

    CALayer * topBorder = [CALayer layer];
    topBorder.borderColor = [[UIColor colorWithHexString:@"DADADA"] CGColor];
    topBorder.borderWidth = 1.0f;
    topBorder.frame = CGRectMake(0, 0, CGRectGetWidth(textViewContainer.frame), CGRectGetHeight(textViewContainer.frame));
    
    [textViewContainer.layer addSublayer:topBorder];
    
    if (parallaxView == nil) {
        parallaxView = [[MDCParallaxView alloc] initWithBackgroundView: backgroundImageView
                                                        foregroundView: textViewContainer];
    }
    parallaxView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-50);
    parallaxView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    parallaxView.backgroundHeight = 250.0f;
    parallaxView.scrollView.scrollsToTop = YES;
    parallaxView.backgroundInteractionEnabled = YES;
    parallaxView.backgroundColor = [UIColor whiteColor];
    parallaxView.scrollViewDelegate = self;

    [self.view addSubview:parallaxView];
    
    UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, parallaxView.frame.origin.y + parallaxView.frame.size.height, 320, 50)];
    lbl.backgroundColor = [UIColor colorWithHexString:@"fffefe"];
    lbl.textColor = [UIColor redColor];
    lbl.text = [NSString stringWithFormat:@"Price - %@",obj.price_label];
    lbl.font = [UIFont fontWithName:@"AvenirNext-Regular" size:18.0f];
    lbl.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lbl];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    parallaxView.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
}

- (void)viewDidDisappear:(BOOL)animated{
    [parallaxView removeFromSuperview];
    parallaxView = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
