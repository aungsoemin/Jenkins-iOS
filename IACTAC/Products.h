//
//  Products.h
//  fyre
//
//  Created by Zayar on 3/7/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Products : NSManagedObject

@property (nonatomic, retain) NSString * cateID;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * detail;
@property (nonatomic, retain) NSString * image_url;
@property (nonatomic, retain) NSNumber * is_active;
@property (nonatomic, retain) NSNumber * isWeb;
@property (nonatomic, retain) NSString * local_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDecimalNumber * price;
@property (nonatomic, retain) NSString * price_label;
@property (nonatomic, retain) NSString * server_id;
@property (nonatomic, retain) NSString * thumb;
@property (nonatomic, retain) NSNumber * timetick;
@property (nonatomic, retain) NSNumber * updated_time;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * can_order;

@end
