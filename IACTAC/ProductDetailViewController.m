//
//  ProductDetailViewController.m
//  IACTAC
//
//  Created by Zayar on 11/13/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "ProductDetailViewController.h"

@interface ProductDetailViewController ()
{
    IBOutlet UIWebView * webView;
}
@end

@implementation ProductDetailViewController
@synthesize objProduct;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [self loadTheViewWith:objProduct];
}

- (void) loadTheViewWith:(Products *)product{
    //[SVProgressHUD showWithStatus:@"Loading Data"];
    NSLog(@"product link %@",product.url);
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",product.url]]]];
    webView.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [SVProgressHUD showErrorWithStatus:@"Having issues connecting to the server"];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    [SVProgressHUD dismiss];
}

@end
