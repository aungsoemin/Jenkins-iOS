//
//  StringTable.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "StringTable.h"


@implementation StringTable

NSString * const DBNAME	= @"nex_pos.sqlite";
NSString * const APP_TITLE = @"Fyre";
NSString * const APP_ID = @"---";
//NSString * const BASE_LINK= @"http://pets.iwh.com.sg/petmobile/";
//NSString * const BASE_LINK= @"http://192.168.2.50:3000/";
//pos nex UAT
NSString * const BASE_LINK= @"http://nex-pos.herokuapp.com/";
//NSString * const BASE_LINK= @"http://pos-nex.herokuapp.com/";
NSString * const CONTACT_LINK = @"api/contacts";
NSString * const ORDER_LINK = @"api/orders";
NSString * const NEWS_LINK = @"api/news";
NSString * const STORE_LINK = @"api/branches";
NSString * const PRODUCT_LINK = @"api/products/";

NSString * const TAB_PRODUCT_ID = @"PRO001";
NSString * const TAB_STORE_ID= @"STO001";
NSString * const TAB_NEWS_ID= @"NEWS001";
NSString * const TAB_INFO_ID= @"INFO001";
NSString * const TAB_CONTACT_ID= @"CONT001";


NSString * const PAYMENT_CASH = @"Cash";
NSString * const PAYMENT_NETS = @"Nets";
NSString * const PAYMENT_VISA = @"Visa/MasterCard";
NSString * const PAYMENT_AMEX = @"AMEX";

NSString * const NEWS_CATE_VIDEO=@"video";
//api/v1/adoptions/browse

NSString * const ANSWER_TYPE_IMAGE = @"I";
NSString * const ANSWER_TYPE_VIDEO = @"V";
NSString * const ANSWER_TYPE_TEXT = @"T";
NSString * const ANSWER_TYPE_NULL = @"null";

NSString * const REPEAT_TYPE_NEVER=@"Never";
NSString * const REPEAT_TYPE_EVER=@"Always";
NSString * const REPEAT_TYPE_ONCE=@"Once";

NSString * const CRYPT_PASS_CODE=@"Ne#Zy";

NSString * const ERRMSG1 = @"Please enter First Name!";
NSString * const ERRMSG2 = @"Please enter Last Name!";
NSString * const ERRMSG3 = @"Please enter User Name!";
NSString * const ERRMSG4 = @"Please enter valid email address!";
NSString * const ERRMSG5 = @"Please enter Password is required!";
NSString * const ERRMSG6 = @"User name and password is incorrect!";
NSString * const ERRMSG7 = @"The email is already exit, Please use another email!";
NSString * const ERRMSG8 = @"Passwords must match!";
NSString * const ERRMSG9 = @"An error occurred. Try again!";
NSString * const ERRMSG10 = @"Please enter Phone No!";
NSString * const ERRMSG11 = @"Your email format is not valid!";
NSString * const ERRMSG12 = @"Please enter your email!";
NSString * const EPRMSG13 = @"Please enter your message!";

NSString * const APN_SERVER_PATH  = @"test.balancedconsultancy.com.sg/tsmaths/apn";

int const STATUS_ACTION_SUCCESS = 1;
int const STATUS_RETURN_RECORD = 3;
int const STATUS_ACTION_FAILED = 2;
int const STATUS_SESSION_EXPIRED = 5;
int const STATUS_NO_RECORD_FOUND = 6;
int const CARD_WIDTH = 160;
int const CARD_HEIGHT = 106;

int const STATUS_REFUND_NO = 0;
int const STATUS_REFUND_FULL = 1;
int const STATUS_REFUND_PARTIAL = 2;
int const STATUS_REFUND_ABSOLUTE = 3;
int const STATUS_REFUND_PARTIAL_ABSOLUTE = 4;

int const STATUS_DISCOUNT_PERCENTAGE = 1;
int const STATUS_DISCOUNT_AMOUNT = 2;
int const STATUS_DISCOUNT_NONE = 0;

NSString * const ANSWER_STATUS_PENDING = @"P";
NSString * const ANSWER_STATUS_ANSWERED = @"A";
NSString * const ANSWER_STATUS_REJECTED = @"R";

float const DEFUALT_TAX= 7.0;
float const DEFUALT_DISCOUNT = 10.0;
BOOL const DEFUALT_SYNC = YES;

double const CACHE_DURATION		= 86400 * 5.0;

double const PROFILE_IMG_VIEW_HEIGHT	= 154;
double const PROFILE_IMG_VIEW_WIDTH	= 137;

int const NORMAL_KEYPAD= 0;
int const DECIMAL_KEYPAD = 1;

int const YEARLY_TIMESTAMP = 30758400;

int const DISCOUNT_TYPE_PERCENTAGE = 1;
int const DISCOUNT_TYPE_AMOUNT = 0;
@end
